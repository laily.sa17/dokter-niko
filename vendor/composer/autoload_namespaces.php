<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'org\\bovigo\\vfs' => array($vendorDir . '/mikey179/vfsStream/src/main/php'),
    'Viocon' => array($vendorDir . '/usmanhalalit/viocon/src'),
    'Valitron' => array($vendorDir . '/vlucas/valitron/src'),
    'Upload' => array($vendorDir . '/codeguy/upload/src'),
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src'),
    'Pixie' => array($vendorDir . '/usmanhalalit/pixie/src'),
    'Gregwar\\Image' => array($vendorDir . '/gregwar/image'),
    'Gregwar\\Cache' => array($vendorDir . '/gregwar/cache'),
);
