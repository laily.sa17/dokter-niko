<?php $__env->startSection('title'); ?>
	OUR PROJECT - <?php echo e($config->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

 <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
                <li><a href="index.html">Home</a></li>
               <li><a href="<?php echo e(base_url()); ?>/main/project">ARTICLE</a></li>
                
              </ul>
            </div>
          </div>
        </div>
      </section>
<div class="container pt-xlg pb-xlg">
		<div class="row">
						<?php foreach($project_view as $result): ?>
						<div class="col-md-4 mb-50px">
						
					
							<span class="thumb-info thumb-info-hide-wrapper-bg">
								<span class="thumb-info-wrapper">
									
										<?php foreach($result->gallery as $image): ?>
										<a href="<?php echo e($result->url); ?>">
									<img src="<?php echo e($image->imagedir); ?>" class="img-responsive" alt="">
									</a>
										<?php break; ?>
                    <?php endforeach; ?>
								</span>
								<?php $t = date('Y-m-d',strtotime ($result->created_at)); ?>
								<span class="thumb-info-wrapper">
									<span class="thumb-info-caption-text">
										<small><i class="fa fa-calendar"></i> <?php echo e(tgl_indo($t)); ?></small>
										<h4><a href="<?php echo e($result->url); ?>"><?php echo read_more($result->title, 35); ?></a></h4>
										<!-- ngeraphin tulisan -->
										<p><?php echo read_more($result->description,200); ?></p>
									</span>
									
								</span>
								<span class="thumb-info-social-icons">
										<a href="<?php echo e($result->url); ?>" class="btn btn-3d btn-tertiary">Read</a>
									</span>
							</span>
						
						</div>
							<?php endforeach; ?>	
						
					</div>
						<ul class="page-numbers">
                 <?php echo $pagination; ?>

                </ul><!-- End Pagination -->
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('website.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>