<?php $__env->startSection('title'); ?>
	<?php echo e($product->name); ?> - <?php echo e($config->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('meta'); ?>
 <meta name="author" content="<?php echo e($config->name); ?>">
<link rel="copyright" href="<?php echo e(base_url()); ?>">
<meta property="og:image" content="<?php echo e($product->imagedir); ?>">
<meta name="keywords" content="<?php echo e($seo->keyword); ?> , <?php echo e($product->category->name); ?>">
<meta name="description" content="Produk Dari Kami <?php echo e($config->name); ?> , <?php echo e($product->name); ?> - Dengan Harga Rp. <?php echo e(number_format($product->price,0,',','.')); ?> , <?php echo e($product->category->name); ?> ">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('styles'); ?>
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
 <div role="main" class="main">
      <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
                <li><a href="<?php echo e(base_url()); ?>">Home</a></li>
                <li class="active"><?php echo e($product->name); ?></li>
                <li>CREATED: <?php echo e($product->created_at); ?></li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <div class="container mb-xlg">
        <div class="row">
          <div class="col-md-4">
            <h2 class="heading-primary text-uppercase font-weight-semibold mb-none"><?php echo e($product->name); ?></h2>
          
            <hr class="solid">
            <div class="text-justify mb-xl">
              <p><?php echo $product->description; ?>

            </div>
            <div class="post-content text-center mt-xl mb-xl">
                      <div>
                        <a href="<?php echo e($product->lresmi); ?>" target="_blank" class="btn btn-3d btn-tertiary btn-block"><?php echo e($product->lresmi); ?></a>
                      </div>
                      <div class="mt-md mb-md">
                        <a href="<?php echo e($product->lgoogleplay); ?>" target="_blank">
                          <img src="<?php echo e(base_url('assets')); ?>/img/logo/google-play.png" class="img-responsive img-center " alt="">
                        </a>
                      </div>
                      <div>
                        <a href="<?php echo e($product->lappstore); ?>" target="_blank">
                          <img src="<?php echo e(base_url('assets')); ?>/img/logo/app-store.png" class="img-responsive img-center " alt="">
                        </a>
                      </div>
                    </div>
          </div>
          <div class="col-md-8">
            <div class="row">
              <?php foreach($product->images as $image): ?>
              <ul class="portfolio-list lightbox m-none" data-plugin-options="{'delegate': 'a.lightbox-portofolio', 'type': 'image', 'gallery': {'enabled': true}}">
                
                <li class="col-md-6 col-sm-6 col-xs-12">
                  <div class="portfolio-item" >
                    <center>  
                    <span class="thumb-info thumb-info-lighten thumb-info-centered-icons">
                      <span class="img-thumbnail">
                        <img src="<?php echo e($image->imagedir); ?>" class="img-responsive"  alt="" style="">
                        <span class="thumb-info-action">
                          <a href="<?php echo e($image->imagedir); ?>" class="lightbox-portofolio">
                            <span class="thumb-info-action-icon thumb-info-action-icon-light">
                              <i class="fa fa-search-plus"></i>
                            </span>
                          </a>
                        </span>
                      </span>
                    </span>
                    </center>
                  </div>
                </li>
                
              </ul>
<?php endforeach; ?>
            </div>
          </div>
        </div>
        <div class="row mt-xlg">

          <div class="col-md-12">
            <div class="heading heading-border heading-middle-border heading-middle-border-center">
              <h2 class="font-weight-semibold text-uppercase">More Article</h2>
            </div>
            <div class="row">
              <?php foreach($project_view as $result): ?>
              <div class="col-md-3">
                <span class="thumb-info thumb-info-lighten thumb-info-bottom-info custom">
                  <span class="thumb-info-wrapper">
                    <?php foreach($result->gallery as $image): ?>
                                        <a href="<?php echo e($result->url); ?>">
                  <img src="<?php echo e($image->imagedir); ?>" class="img-responsive" alt="">
                  </a>
                    <?php break; ?>
                    <?php endforeach; ?>
                    <span class="thumb-info-title">
                      <span class="thumb-info-inner"><a href="#"><?php echo e($result->title); ?></a></span>
                    </span>
                  </span>
                </span>
              </div>
<?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
<script>
 $("#shareIcons").jsSocials({ 
    showLabel: false,
    showCount: false,
    shares: ["twitter", "facebook", "whatsapp","googleplus","line"]
  });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('website.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>