<?php $__env->startSection('title'); ?>
	<?php echo e($project->title); ?> - <?php echo e($config->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('meta'); ?>
 <meta title="author" content="<?php echo e($config->title); ?>">
<link rel="copyright" href="<?php echo e(base_url()); ?>">
<meta property="og:image" content="<?php echo e($project->imagedir); ?>">
<meta title="keywords" content="<?php echo e($seo->keyword); ?> , project">
<meta title="description" content="Our Project , <?php echo e(read_more($project->description,200)); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('styles'); ?>
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <div role="main" class="main">
      <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
                <li><a href="<?php echo e(base_url()); ?>">Home</a></li>
                <li><a href="<?php echo e(base_url()); ?>/main/project">ARTICLE</a></li>
                <li class="active"><?php echo e($project->title); ?></li>
                
              </ul>
            </div>
          </div>
        </div>
      </section>
      <div class="container">
        <div class="row">
          <div class="col-md-9">
            <div class="blog-posts single-post">
              <article class="post post-large blog-single-post">
                <div class="post-image">

                  <div class="owl-carousel owl-theme" data-plugin-options="{'items': 1}">
                   <?php foreach($project->gallery as $image): ?>
                    <div>
                      <center>
                      <div class="img-thumbnail">

                        <img src="<?php echo e($image->imagedir); ?>" class="img-responsive" alt="" >
                       
                      </div>
                      </center>
                    </div>
                    
                    <?php endforeach; ?>
                  </div>
                </div>
              <div class="post-date">
                <!-- fungsi ada di aksa_helper (helper) -->
                <?php
                  $create = explode('-',$project->created_at);
                  
                  $thn    = $create[0];
                  $bln    = $create[1];
                // menampilkan array

// echo $data = date('Y-m-d',strtotime($timestamp));
// echo '<br>';
// echo $time = date('H:i:s',strtotime($timestamp));
//                   2012-10-19
// 18:19:56

// array 2
                  $tgl    = $create[2];
                  // nampilno tgl e tok
                  $tanggal = date('d',strtotime($project->created_at));
                
                 
                ?>
                  <span class="day"><?php echo e(($tanggal)); ?></span>
                  <span class="month"><?php echo e(bln_indo($bln)); ?></span>
                  <span class="year"><?php echo e($thn); ?></span>
                </div>
                <div class="post-content">
                  <h2><?php echo e($project->title); ?></h2>
                  <div class="post-meta">
                   
                  </div>
                  <p><?php echo $project->description; ?>.</p>
                </div>
                <div class="post-block" id="share_post"></div>
              </article>
            </div>
          </div>
          <div class="col-md-3">
            <aside class="sidebar">
              <form action="<?php echo e(base_url('main/search')); ?>" method="get">
                <div class="input-group">
                  <input type="search" name="q" class="form-control" placeholder="Search...">
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                  </span>
                </div>
              </form>
  
            </aside>
            <br>
            <br>
            <br>

          </div>
        </div>
      
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
<script>
 $("#shareIcons").jsSocials({ 
    showLabel: false,
    showCount: false,
    shares: ["twitter", "facebook", "whatsapp","googleplus","line"]
  });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('website.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>