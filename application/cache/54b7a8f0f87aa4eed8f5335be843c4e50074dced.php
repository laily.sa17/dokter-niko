<?php $__env->startSection('title'); ?>
	OUR PROMO - <?php echo e($config->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <div id="page-title" class="page-title-parallax text-light" data-stellar-background-ratio="0.9" style="background-image: url('<?php echo e(base_url()); ?>images/website/banner.jpg')">
    <div class="background-overlay"></div>
    <div class="container">
      <div class="page-title col-md-8">
        <h1 class="text-uppercase text-medium">promo</h1>
      </div>
    </div>
  </div>

  <section class="content">
    <div class="container">
      <div class="row">
        <div class="post-content post-thumbnail col-md-9">
          <div class="post-item">
          	<?php foreach($promos as $result): ?>
            <div class="post-content-details">
              <div class="post-title">
                <h3><a href="#"><?php echo e($result->name); ?></a></h3>
                <?php foreach($result->promo->where("status",0) as $toko): ?>
                <h4><a href="<?php echo e($toko->url); ?>"><?php echo e($toko->name); ?></a></h4>
                <?php endforeach; ?>
              </div>

              <div class="post-description">
                &nbsp;               
              </div>

            </div>
            <?php endforeach; ?>
          </div>
        </div>
        <div class="sidebar sidebar-modern col-md-3">
        <?php echo $__env->make('website.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
      </div>
    </div>
  </section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('website.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>