<?php $__env->startSection('title'); ?>
	<?php echo e($service->name); ?> - <?php echo e($seo->title); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('meta'); ?>
 <meta name="author" content="<?php echo e($config->name); ?>">
<link rel="copyright" href="<?php echo e(base_url()); ?>">
<meta property="og:image" content="<?php echo e($service->imagedir); ?>">
<meta name="keywords" content="<?php echo e($seo->keyword); ?> , service">
<meta name="description" content="Our Service , <?php echo e(read_more($service->description,200)); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('styles'); ?>
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <div role="main" class="main">
      <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
                <li><a href="index.html">Detail</a></li>
                <li class="active"><?php echo e($service->name); ?></li>
                <li>created: <?php echo e($service->created_at); ?></li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <div class="container">
        <div class="row">
          <div class="col-md-9">
            <div class="blog-posts single-post">
              <article class="post post-large blog-single-post">
                <div class="post-image">
                  <div class="owl-carousel owl-theme" data-plugin-options="{'items': 1}">
                    <div>
                      <div class="img-thumbnail">
                        <img src="https://picsum.photos/g/1280/500?image=0" class="img-responsive" alt="">
                      </div>
                    </div>
                    <div>
                      <div class="img-thumbnail">
                        <img src="https://picsum.photos/g/1280/500?image=1" class="img-responsive" alt="">
                      </div>
                    </div>
                    <div>
                      <div class="img-thumbnail">
                        <img src="https://picsum.photos/g/1280/500?image=2" class="img-responsive" alt="">
                      </div>
                    </div>
                  </div>
                </div>

                <div class="post-content">
                  <h2><?php echo e($service->name); ?></h2>
                  <div class="post-meta">
                    <span><i class="fa fa-tag"></i> <a href="#">Tag 1</a>, <a href="#">Tag 2</a></span>
                  </div>
                  <p><?php echo $service->description; ?></p>
                </div>
                <div class="post-block" id="shareIcons"></div>
              </article>
            </div>
          </div>
          <div class="col-md-3">
            <aside class="sidebar">
              <form>
                <div class="input-group">
                  <input type="text" name="q" class="form-control" placeholder="Cari...">
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                  </span>
                </div>
              </form>
              <hr>
              <h4 class="heading-primary">Kategori</h4>
              <ul class="nav nav-list mb-xlg">
                <li><a href="#">Kategori 1</a></li>
                <li><a href="#">Kategori 2</a></li>
                <li><a href="#">Kategori 3</a></li>
                <li><a href="#">Kategori 4</a></li>
                <li><a href="#">Kategori 5</a></li>
              </ul>
            </aside>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
<script>
 $("#shareIcons").jsSocials({ 
    showLabel: false,
    showCount: false,
    shares: ["twitter", "facebook", "whatsapp","googleplus","line"]
  });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('website.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>