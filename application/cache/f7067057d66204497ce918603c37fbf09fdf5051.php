<?php $__env->startSection('title'); ?>
	Searching '<?php echo e($ctrl->input->get('q')); ?>'
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>
<div id="core" class="core-bg-1">
      <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="index.html">Search</a></li>

                 <li class="active"><?php echo e(ucwords($_GET['q'])); ?></li>
              </ul>
            </div>
          </div>
        </div>
      </section>
	<!-- PAGE HEADER : begin -->


	<div class="container">
					<div class="row">

						<div class="col-md-6" style="margin-left: 25%">
						<center>
   
						<form class="c-search-form" action="<?php echo e(base_url('main/search')); ?>" method="get">
							
							<div class="input-group">
							
								<input  type="search" name="q" placeholder="Search Something Here" class="form-control" value="<?php echo e($ctrl->input->get('q')); ?>">
								  <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                   
                  </span>
                  

							</div>
							
						</form>
</center>
					
			</div>
		
					</div>
				</div>
		
			</div>

						
						<!-- SEARCH FORM : end -->
						<br>
						<br>
						<br>
						<div class="b">
						<div class="container">
<div class="row">
						<!-- DIVIDER : begin -->
						<hr class="c-divider m-medium">
						<!-- DIVIDER : end -->

						<!-- RESULTS LIST : begin -->
						<ul class="results-list">
				<?php foreach($project_view as $result): ?>
						<div class="col-md-4 mb-50px">
							<span class="thumb-info thumb-info-hide-wrapper-bg">
								<span class="thumb-info-wrapper">
									
										<?php foreach($result->gallery as $image): ?>
										<a href="<?php echo e($result->url); ?>">
									<img src="<?php echo e($image->imagedir); ?>" class="img-responsive" alt="">
									</a>
										<?php break; ?>
                    			<?php endforeach; ?>
								</span>
								<?php $t = date('Y-m-d',strtotime($result->created_at)); ?>
								<span class="thumb-info-wrapper">
									<span class="thumb-info-caption-text">
										<small><i class="fa fa-calendar"></i> <?php echo e(tgl_indo($t)); ?></small>
										<h4><a href="<?php echo e($result->url); ?>"><?php echo read_more($result->title, 35); ?></a></h4>
									<p><?php echo read_more($result->description,200); ?></p>
									</span>
									
								</span>
								<span class="thumb-info-social-icons">
										<center><a href="<?php echo e($result->url); ?>" class="btn btn-3d btn-tertiary">Read</a></center>
									</span>
							</span>
						</div>
							<?php endforeach; ?>	
							
							<?php foreach($product as $result): ?>
						<div class="col-md-4 mb-50px">
						
							<span class="thumb-info thumb-info-hide-wrapper-bg">
								<span class="thumb-info-wrapper">
									
										<?php foreach($result->images as $image): ?>
										<a href="<?php echo e($result->url); ?>">
									<img src="<?php echo e($image->imagedir); ?>" class="img-responsive" alt="">
									</a>
										<?php break; ?>
                   					 <?php endforeach; ?>
								</span>
								<?php $t = date('Y-m-d',strtotime($result->created_at)); ?>
								<span class="thumb-info-wrapper">
									<span class="thumb-info-caption-text">
										<small><i class="fa fa-calendar"></i> <?php echo e(tgl_indo($t)); ?></small>
										<h4><a href="<?php echo e($result->url); ?>"><?php echo read_more($result->name, 35); ?></a></h4>
									<p><?php echo read_more($result->description,200); ?></p>
									</span>
									
								</span>
								<span class="thumb-info-social-icons">
										<center><a href="<?php echo e($result->url); ?>" class="btn btn-3d btn-tertiary">Read</a></center>
									</span>
							</span>
						
						</div>
							<?php endforeach; ?>	

							
						
						</ul>
					</div>
					</div>
				</div>
				</div>
			</div>

</div>

<!-- CORE : end -->

<!-- BOTTOM PANEL : begin -->
<!-- BOTTOM PANEL : end -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('website.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>