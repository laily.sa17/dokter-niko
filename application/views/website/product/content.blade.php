@extends('website.template')
@section('title')
	{{$product->name}} - {{$config->name}}
@endsection

@section('meta')
 <meta name="author" content="{{$config->name}}">
<link rel="copyright" href="{{base_url()}}">
<meta property="og:image" content="{{$product->imagedir}}">
<meta name="keywords" content="{{$seo->keyword}} , {{$product->category->name }}">
<meta name="description" content="Produk Dari Kami {{$config->name}} , {{$product->name}} - Dengan Harga Rp. {{number_format($product->price,0,',','.')}} , {{$product->category->name }} ">
@endsection

@section('styles')
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
@endsection

@section('content')
 <div role="main" class="main">
      <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
                <li><a href="{{base_url()}}">Home</a></li>
                <li class="active">{{$product->name}}</li>
                <li>CREATED: {{$product->created_at}}</li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <div class="container mb-xlg">
        <div class="row">
          <div class="col-md-4">
            <h2 class="heading-primary text-uppercase font-weight-semibold mb-none">{{$product->name}}</h2>
          
            <hr class="solid">
            <div class="text-justify mb-xl">
              <p>{!!$product->description!!}
            </div>
            <div class="post-content text-center mt-xl mb-xl">
                      <div>
                        <a href="{{$product->lresmi}}" target="_blank" class="btn btn-3d btn-tertiary btn-block">{{$product->lresmi}}</a>
                      </div>
                      <div class="mt-md mb-md">
                        <a href="{{$product->lgoogleplay}}" target="_blank">
                          <img src="{{base_url('assets')}}/img/logo/google-play.png" class="img-responsive img-center " alt="">
                        </a>
                      </div>
                      <div>
                        <a href="{{$product->lappstore}}" target="_blank">
                          <img src="{{base_url('assets')}}/img/logo/app-store.png" class="img-responsive img-center " alt="">
                        </a>
                      </div>
                    </div>
          </div>
          <div class="col-md-8">
            <div class="row">
              @foreach($product->images as $image)
              <ul class="portfolio-list lightbox m-none" data-plugin-options="{'delegate': 'a.lightbox-portofolio', 'type': 'image', 'gallery': {'enabled': true}}">
                
                <li class="col-md-6 col-sm-6 col-xs-12">
                  <div class="portfolio-item" >
                    <center>  
                    <span class="thumb-info thumb-info-lighten thumb-info-centered-icons">
                      <span class="img-thumbnail">
                        <img src="{{$image->imagedir}}" class="img-responsive"  alt="" style="">
                        <span class="thumb-info-action">
                          <a href="{{$image->imagedir}}" class="lightbox-portofolio">
                            <span class="thumb-info-action-icon thumb-info-action-icon-light">
                              <i class="fa fa-search-plus"></i>
                            </span>
                          </a>
                        </span>
                      </span>
                    </span>
                    </center>
                  </div>
                </li>
                
              </ul>
@endforeach
            </div>
          </div>
        </div>
        <div class="row mt-xlg">

          <div class="col-md-12">
            <div class="heading heading-border heading-middle-border heading-middle-border-center">
              <h2 class="font-weight-semibold text-uppercase">More Article</h2>
            </div>
            <div class="row">
              @foreach($project_view as $result)
              <div class="col-md-3">
                <span class="thumb-info thumb-info-lighten thumb-info-bottom-info custom">
                  <span class="thumb-info-wrapper">
                    @foreach($result->gallery as $image)
                                        <a href="{{$result->url}}">
                  <img src="{{$image->imagedir}}" class="img-responsive" alt="">
                  </a>
                    @break
                    @endforeach
                    <span class="thumb-info-title">
                      <span class="thumb-info-inner"><a href="#">{{$result->title}}</a></span>
                    </span>
                  </span>
                </span>
              </div>
@endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
<script>
 $("#shareIcons").jsSocials({ 
    showLabel: false,
    showCount: false,
    shares: ["twitter", "facebook", "whatsapp","googleplus","line"]
  });
</script>
@endsection