<!DOCTYPE html>
<html lang="en">
<head>

<title>@yield('title',$seo->title)</title>
@yield('meta','<meta name="keywords" content="'.$seo->keyword.'">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="'.$seo->description.'">
<meta name="author" content="'.$config->name.'">
<meta property="og:image" content="'.$seo->imagedir.'">')

<link rel="icon" href="{{$config->icondir}}" type="image/x-icon" />
<link rel="shortcut icon" href="{{$config->icondir}}" type="image/x-icon" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light">

  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/vendor/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/vendor/animate/animate.min.css">
  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/vendor/simple-line-icons/css/simple-line-icons.min.css">
  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/vendor/owl.carousel/assets/owl.carousel.min.css">
  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/vendor/owl.carousel/assets/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/vendor/magnific-popup/magnific-popup.min.css">

  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/css/theme.css">
  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/css/theme-elements.css">
  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/css/theme-blog.css">
  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/css/theme-shop.css">

  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/vendor/rs-plugin/css/settings.css">
  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/vendor/rs-plugin/css/layers.css">
  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/vendor/rs-plugin/css/navigation.css">
  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/vendor/circle-flip-slideshow/css/component.css">

  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/css/skins/default.css">

  <link rel="stylesheet" type="text/css" href="{{base_url('assets')}}/css/custom.css">

  <script type="text/javascript" src="{{base_url('assets')}}/vendor/modernizr/modernizr.min.js"></script>


<!-- config -->
<link rel="stylesheet" type="text/css" href="{{base_url('admin_assets')}}/css/aksa.css">


@yield('styles')
<style type="text/css">
.topbar.topbar_colored {
  background: #fff;
  border-bottom: none;
}

.top_details > span > a {
  text-transform: uppercase;
}
.top_details > span, .top_details > div {
  display: inline-block;
  padding-top: 12px;
  font-size: 11px;
}
.top_details .title {
  margin: 0 5px 0 0px;
  text-transform: uppercase;
}

/* popup*/
  #tableiklan {
     height:100%;
     width: 100%;
     margin: 0;
     padding: 0;
     border: 0;
  }
  #tableiklan td {
     vertical-align: middle;
     text-align: center;
  }
  .popup-close {
    right: 10em
  }

.f_left {
  float: left;
}
.f_right {
    float: right;
}
.top_expande, .not_expanded {
  background-color: red;
}

@media only screen and (max-width: 768px) {
  .top_details {
    margin-left: 0;
  }
  .topbar.topbar_small {
    padding: 10px 0;
  }
  .top_details, .top-socials {
    float: none;
    text-align: center;
    margin: 0;
  }
}

@media only screen and (max-width: 480px) {
  .top_details {
    float: none;
    margin-left: 0;
    text-align: center;
  }
}
</style>

</head>

<body>

    <header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 150, 'stickySetTop': '-100px'}">
      <div class="header-body">
        <div class="header-container container">
          <div class="header-row">
            <div class="header-column header-column-center">
              <div class="header-logo">
                <a href="{{base_url()}}">
                  <img src="{{$config->logodir}}" width="111"  alt="Logo Website Dokter Niko">
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="header-container container header-nav header-nav-center">
          <button type="button" class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
            <i class="fa fa-bars"></i>
          </button>
          <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
            <nav>
              <ul class="nav nav-pills" id="mainNav">
                @foreach($menupengunjung as $menus)
                  @if(count($menus->submenupengunjung) !=0)
                  <li class="dropdown">
                    <a href="{{ base_url().''.$menus->link }}" class="{{match($menu,$menus->menu,'active')}}">
                    {{$menus->judul}} <i class="fa fa-angle-down"></i></a>
                    <ul class="dropdown-menu">
                      @foreach($menus->submenupengunjung->where('aktif','0') as $sub)
                      <li>
                      @if($sub->id =='8')
                      <a href="{{$sub->link}}" target="_blank">{{$sub->judul}}
                      @else
                      <a href="{{base_url().''.$sub->link}}">{{$sub->judul}}
                      @endif
                      </a>
                      </li>
                      @endforeach
                    </ul>
                  </li>
                  @else
                   <li><a href="{{ base_url().''.$menus->link }}">{{$menus->judul}}</a></li>
                  @endif
                @endforeach
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </header>

      <!-- ./Slider -->
  @yield('content')
  <!-- BOTTOM PANEL : end -->
    <!-- Footer -->
    <!-- <footer class="background-dark text-grey" id="footer">
      <div class="footer-content">
        <div class="container">
          <div class="row">
            <div class="col-md-3">
              <div class="widget clearfix widget-contact-us">                
                <img src="{{$config->logowhitedir}}" title="{{$config->name}}" alt="{{$config->name}}" class="img-responsive">
              </div>
            </div>
            <div class="col-md-3">
              <div class="widget clearfix widget-contact-us" style="background-image: url('{{base_url()}}images/website/world-map.png'); background-position: 50% 55px; background-repeat: no-repeat">
                <h4 class="widget-title text-uppercase">Contact Us</h4>
                <ul class="list-large list-icons">
                  <li><i class="fa fa-envelope"></i> {{$config->email}}</li>
                  <li><i class="fa fa-map-marker"></i> {{$config->address}}</li>
                  <li><i class="fa fa-phone"></i> {{$config->contact}}</li>
                </ul>
              </div>
            </div>

            <div class="col-md-3 height-col-md-footer">
              <div class="widget clearfix widget-newsletter">
                <h4>Newsletter</h4>
                <form id="widget-subscribe-form" action="{{base_url('main/subscribe')}}" role="form" class="form-inline">
                  <p style="margin:0;">Subscribe to our Newsletter</p>
                  <div class="input-group"> -->
                    <!-- <span class="input-group-addon"><i class="fa fa-paper-plane"></i></span> -->
                <!--     <input type="email" name="email" class="form-control required email" placeholder="Enter your Email" required>
                    <span class="input-group-btn">
                      <button type="submit" id="widget-subscribe-submit-button" class="btn btn-white">Subscribe</button>
                    </span>
                  </div>
                </form>
              </div>
            </div>
            <div class="col-md-3 height-col-md-footer">


              
              <div class="widget clearfix widget-newsletter">
                <h4>Visitor Statistics</h4>
                <ul class="list-large" style="list-style: none;">
            <li style="margin: 5px 2px;"><img src="{{base_url()}}images/website/pusat-grosir-1.png" style="height:21px;">  Hari Ini    : {{$pengunjung}} Orang </li>
            <li style="margin: 5px 2px;"><img src="{{base_url()}}images/website/pusat-grosir-2.png" style="height:21px;"> Kemarin     : {{$kemarin}} Orang</li>
            <li style="margin: 5px 2px;"><img src="{{base_url()}}images/website/pusat-grosir-3.png" style="height:21px;"> Bulan Ini   : {{$perbulan}} Orang</li>
            <li style="margin: 5px 2px;"><img src="{{base_url()}}images/website/pusat-grosir-4.png" style="height:21px;"> Tahun Ini   : {{$pertahun}} Orang</li>
            <li style="margin: 5px 2px;"><img src="{{base_url()}}images/website/pusat-grosir-5.png" style="height:21px;"> Total       : {{$totalpengunjung}} Orang</li>
            <li style="margin: 5px 2px;"><img src="{{base_url()}}images/website/pusat-grosir-6.png" style="height:21px;"> Now Online  : {{$online}} Orang</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright-content">
        <div class="container">
          <div class="row">
            <div class="copyright-text col-md-6 col-sm-12 col-xs-12">
              &copy; 2017. Developed by <a href="http://aksamedia.co.id" target="_blank">Jasa Pembuatan Website</a>.
            </div>
            <div class="col-md-6">
              <div class="social-icons">
                <ul>
                @foreach($sosmed as $result)
                  <li class="social-{{$result->type}}"> <a href="{{$result->url}}" target="_blank" title="{{$result->name}}">
                    <i class="fa fa-{{$result->type}}"></i></a>
                  </li>
                @endforeach
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer> -->
        <!-- Copyright -->
  <footer  style="background-color: #2f93a2ff">
    <CENTER>
        <div id="copyright" role="contentinfo">
        <div class="container">
          <br>
            <p class="nav nav-pills" id="mainNav"style="color: #ffff">&copy; 2017 Dokter-Niko All rights reserved. <a href="#" target="_blank">Jasa Pembuatan Website</a></p>
            <br>
        </div>
    </div><!-- End Copyright -->
  </CENTER>
  </footer>
    <!-- End Copyright -->
    <!-- ./Footer --> 

  </div>
  @if($popup->aktif==0)
  <div class="modal fade" id="modaliklan" style="background: rgba(0, 0, 0, 0.5);" data-dismiss="modal">
    
    <table id="tableiklan">
      <tr>
        <td>
          <a class="popup-close pull-right btn btn-danger" data-dismiss="modal">X</a><a class="text-center" href="{{$popup->url}}" target="_blank">
          <img style="margin: 0 auto;" alt="{{$popup->title}}" src="{{$popup->imagedir}}" class="img-responsive">
          </a>
      </td>
      </tr>
    </table>
  </div>
  @endif
<!-- JavaScript --> 
<script type="text/javascript" src="{{base_url('assets')}}/vendor/jquery/jquery.min.js"></script>
  <script type="text/javascript" src="{{base_url('assets')}}/vendor/jquery.appear/jquery.appear.min.js"></script>
  <script type="text/javascript" src="{{base_url('assets')}}/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script type="text/javascript" src="{{base_url('assets')}}/vendor/jquery-cookie/jquery-cookie.min.js"></script>
  <script type="text/javascript" src="{{base_url('assets')}}/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="{{base_url('assets')}}/vendor/common/common.min.js"></script>
  <script type="text/javascript" src="{{base_url('assets')}}/vendor/jquery.validation/jquery.validation.min.js"></script>
  <script type="text/javascript" src="{{base_url('assets')}}/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
  <script type="text/javascript" src="{{base_url('assets')}}/vendor/jquery.gmap/jquery.gmap.min.js"></script>
  <script type="text/javascript" src="{{base_url('assets')}}/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
  <script type="text/javascript" src="{{base_url('assets')}}/vendor/isotope/jquery.isotope.min.js"></script>
  <script type="text/javascript" src="{{base_url('assets')}}/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script type="text/javascript" src="{{base_url('assets')}}/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
  <script type="text/javascript" src="{{base_url('assets')}}/vendor/vide/vide.min.js"></script>

  <script type="text/javascript" src="{{base_url('assets')}}/js/theme.js"></script>

  <script type="text/javascript" src="{{base_url('assets')}}/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="{{base_url('assets')}}/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="{{base_url('assets')}}/js/bootstrap.js"></script>
  
  <script type="text/javascript" src="{{base_url('assets')}}/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js"></script>
  <script type="text/javascript" src="{{base_url('assets')}}/js/views/view.home.js"></script>

  <script type="text/javascript" src="{{base_url('assets')}}/js/custom.js"></script>

  <script type="text/javascript" src="{{base_url('assets')}}/js/theme.init.js"></script>


 
  <script type="text/javascript" src="{{base_url('admin_assets')}}/js/aksa/aksa-js.js"></script>
  <script type="text/javascript">
    function getCookie(name) {
        var dc = document.cookie;
        var prefix = name + "=";
        var begin = dc.indexOf("; " + prefix);
        if (begin == -1) {
            begin = dc.indexOf(prefix);
            if (begin != 0) return null;
        }
        else
        {
            begin += 2;
            var end = document.cookie.indexOf(";", begin);
            if (end == -1) {
            end = dc.length;
            }
        }
        return decodeURI(dc.substring(begin + prefix.length, end));
    } 
    $('.popup-close').click(function() {
          document.cookie = "zamoraclose=1";
          $('.popup-close').fadeOut();
          $('#modaliklan').modal('hide');
      });
    $(document).ready(function(){
      if(getCookie('zamoraclose') == null){
        $('#modaliklan').modal('show');
      }
    });
  </script>

  <!-- Facebook Pixel-->
  @if (!empty($seo->fbpixel))
  {!! $seo->fbpixel !!}
  @endif


  <!-- google analytics-->
  @if (!empty($seo->analytic))
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', '{{$seo->analytic}}', 'auto');
  ga('send', 'pageview',location.pathname);
  ga('require', 'ecommerce');
  </script>
  @endif
@yield('script')
</body>
</html>