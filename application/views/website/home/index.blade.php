@extends('website.template')
@section('title')
	{{$config->name}}
@endsection

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css">
<style type="text/css">
  .videoyoutube{
      text-align: center;
      background:linear-gradient(to bottom, #fefefe 0%, #d1d1d1 100%);
      border:1px solid #A8A8A8;
      border-radius:5px;
      margin:20px auto;
      width:90%;
      box-shadow: 1px 30px 30px -26px #818181;
  }
  .video-responsive {
    position: relative;
    padding-bottom: 56.25%;
    height: 0;
    overflow: hidden;
    margin:8px;
  }
  .video-responsive iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border:0;
  }
</style>
	<!-- nivo slider -->
    <link rel="stylesheet" href="{{base_url('assets')}}/nivoslider/default/default.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{base_url('assets')}}/nivoslider/nivo-slider.css" type="text/css" media="screen" />
@endsection

@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
<script type="text/javascript">
    jQuery("a[rel=group]").click(function() {
            jQuery.fancybox({
                    'padding'       : 0,
                    'autoScale'     : false,
                    'transitionIn'  : 'none',
                    'transitionOut' : 'none',
                    'title'         : this.title,
                    'width'     	: 680,
                    'height'        : 495,
                    'href'          : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
                    'type'          : 'swf',
                    'swf'           : {
                         'wmode'        : 'transparent',
                        'allowfullscreen'   : 'true'
                    }
                });

            return false;
        });

	setTimeout(function(){
		$('.video-youtube').each(function(){
		$(this).replaceWith('<iframe class="video-youtube loader" src="'+$(this).data('src')+'" allowfullscreen="allowfullscreen" height="281" width="500"></iframe>')})
	},5000);
</script>
<script type="text/javascript" src="{{base_url('assets')}}/nivoslider/jquery.nivo.slider.js"></script>
<script type="text/javascript">
$(window).load(function() {

    $('#slider').nivoSlider({ 
        effect: 'random',                 // Specify sets like: 'fold,fade,sliceDown' 
        slices: 15,                       // For slice animations 
        boxCols: 8,                       // For box animations 
        boxRows: 4,                       // For box animations 
        animSpeed: 500,                   // Slide transition speed 
        pauseTime: 3000,                  // How long each slide will show 
        startSlide: 0,                    // Set starting Slide (0 index) 
        directionNav: true,               // Next & Prev navigation 
        controlNav: false,                 // 1,2,3... navigation 
        controlNavThumbs: false,          // Use thumbnails for Control Nav 
        pauseOnHover: true,               // Stop animation while hovering 
        manualAdvance: false,             // Force manual transitions 
        randomStart: false,               // Start on a random slide 
        beforeChange: function(){},       // Triggers before a slide transition 
        afterChange: function(){},        // Triggers after a slide transition 
        slideshowEnd: function(){},       // Triggers after all slides have been shown 
        lastSlide: function(){},          // Triggers when last slide is shown 
        afterLoad: function(){}           // Triggers when slider has loaded 
    });

});
</script>
@endsection
@section('content')

      <!-- Slider -->
      <div style="background-color: #298995ff;">

		<div class="col-md-12" style="background-color: #298995ff;">
							@foreach($service as $result)	
							<article class="post post-medium">
								<div class="row">
									<div class="col-md-3">
										<div class="post-image">
	
											<div>
												
												<div>
													
													
													
													<div >
													
											
														<img src="{{$result->imagedir}}" class="img-responsive" alt="">
														
										</div>
									
												</div>
											
											
											</div>
											
										</div>
									</div>

									<div class="col-md-4">
										<div class="post-content text-justify">
										<h1 style="color: #ffff; margin: 0 0 0 0 "><b>{{$result->name}}</b></h1>

										<p style="color: #ffff;">{{$result->description_sort}}</p>
										<p style="color: #ffff;">{{$result->kontak}}</p>
										<p>{!!$result->description!!} </p>
											
										</div>
									</div>
									<div class="col-md-5">
<center><!-- InstaWidget -->
<a href="https://instawidget.net/v/user/andhika.tri" id="link-1cd739eea46a1d14e4c397a17aae954495e238e425923fa466dcd6a691d05b7a">@andhika.tri</a>
<script src="https://instawidget.net/js/instawidget.js?u=1cd739eea46a1d14e4c397a17aae954495e238e425923fa466dcd6a691d05b7a&width=325px"></script></center>
									</div>
								</div>
							</article>
							@endforeach
						</div>
					</div>
				</div>

					<!-- InstaWidget -->

				</div>
			</div>

    <!-- Article -->
			<div class="bg-grey">
				<div class="container pt-xlg pb-xlg">
					<div class="row">
						<div class="col-md-12 center mb-md">
							<br>
							<br>
							<h4 class="font-weight-semibold">Article</h4>
						</div>
					</div>
					<div class="row">
						@foreach($project_view as $result)
						<div class="col-md-4 mb-50px">
						
					
							<span class="thumb-info thumb-info-hide-wrapper-bg">
								<span class="thumb-info-wrapper">
									
										@foreach($result->gallery as $image)
										<a href="{{$result->url}}">
									<img src="{{$image->imagedir}}" class="img-responsive" alt="">
									</a>
										@break
                    @endforeach
								</span>
								<?php $t = date('Y-m-d',strtotime($result->created_at)); ?>
								<span class="thumb-info-wrapper">
									<span class="thumb-info-caption-text">
										<small><i class="fa fa-calendar"></i> {{tgl_indo($t)}}</small>
										<h4><a href="{{$result->url}}">{!!read_more($result->title, 35)!!}</a></h4>
									<p>{!! read_more($result->description,200) !!}</p>
									</span>
									
								</span>
								<span class="thumb-info-social-icons">
										<center><a href="{{$result->url}}" class="btn btn-3d btn-tertiary">Read</a></center>
									</span>
							</span>
						
						</div>
							@endforeach	
					</div>
					<center><a href="main/project" class="btn btn-3d btn-tertiary">View More</a></center>
				</div>

			</div>
			<!-- ./Article -->

			<!-- Product Applications -->
			<div class="container pt-xlg pb-xlg">
				<div class="row">
					<div class="col-md-12 center mb-md">
						<h4 class="font-weight-semibold">Product Applications</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="blog-posts">
							@foreach($product as $result)	
							<article class="post post-medium">
								<div class="row">
									<div class="col-md-3">
										<div class="post-image " >
	
											<div class="owl-carousel owl-theme" data-plugin-options="{'items': 1}  " >
												@foreach($result->images as $image)
												<div>
													
													
													
													<div class="img-thumbnail">
													
											
														<img src="{{$image->imagedir}}" class="img-responsive" alt="">
														
										</div>
									
												</div>
											
												@endforeach
											</div>
											
										</div>
									</div>

									<div class="col-md-6">
										<div class="post-content text-justify">
											<a href="{{$result->url}}">
												<img src="{{$result->icondir}}" class="img-responsive mb-xl mt-xl" alt="" style="height: 50px; max-height: 50px;">
											</a>
											<p>{!! read_more($result->description,1000) !!}</p>
										</div>
										<br>
										<span >
										<a href="{{$result->url}}" class="btn btn-3d btn-tertiary">Read</a>
									</span>
									</div>
									<div class="col-md-3">
										<div class="post-content text-center mt-xl mb-xl">
											<div>
												<a href="{{$result->lresmi}}" target="_blank" class="btn btn-3d btn-tertiary btn-block">{{$result->lresmi}}</a>
											</div>
											<div class="mt-md mb-md">
												<a href="{{$result->lgoogleplay}}" target="_blank">
													<img src="{{base_url('assets')}}/img/logo/google-play.png" class="img-responsive img-center img-fullwidth" alt="">
												</a>
											</div>
											<div>
												<a href="{{$result->lappstore}}" target="_blank">
													<img src="{{base_url('assets')}}/img/logo/app-store.png" class="img-responsive img-center img-fullwidth" alt="">
												</a>
											</div>

										</div>
									</div>
								</div>
							</article>
							@endforeach
						</div>
					</div>
				</div>
			</div>
			<!-- ./Product Applications -->

		</div>
	</div>
@endsection