@extends('website.template')
@section('title')
	OUR PROJECT - {{$config->name}}
@endsection

@section('content')

 <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
                <li><a href="index.html">Home</a></li>
               <li><a href="{{base_url()}}/main/project">ARTICLE</a></li>
                
              </ul>
            </div>
          </div>
        </div>
      </section>
<div class="container pt-xlg pb-xlg">
		<div class="row">
						@foreach($project_view as $result)
						<div class="col-md-4 mb-50px">
						
					
							<span class="thumb-info thumb-info-hide-wrapper-bg">
								<span class="thumb-info-wrapper">
									
										@foreach($result->gallery as $image)
										<a href="{{$result->url}}">
									<img src="{{$image->imagedir}}" class="img-responsive" alt="">
									</a>
										@break
                    @endforeach
								</span>
								<?php $t = date('Y-m-d',strtotime ($result->created_at)); ?>
								<span class="thumb-info-wrapper">
									<span class="thumb-info-caption-text">
										<small><i class="fa fa-calendar"></i> {{tgl_indo($t)}}</small>
										<h4><a href="{{$result->url}}">{!!read_more($result->title, 35)!!}</a></h4>
										<!-- ngeraphin tulisan -->
										<p>{!! read_more($result->description,200) !!}</p>
									</span>
									
								</span>
								<span class="thumb-info-social-icons">
										<a href="{{$result->url}}" class="btn btn-3d btn-tertiary">Read</a>
									</span>
							</span>
						
						</div>
							@endforeach	
						
					</div>
						<ul class="page-numbers">
                 {!! $pagination !!}
                </ul><!-- End Pagination -->
</div>
@endsection