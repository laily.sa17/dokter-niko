@extends('website.template')
@section('title')
	{{$project->title}} - {{$config->name}}
@endsection

@section('meta')
 <meta title="author" content="{{$config->title}}">
<link rel="copyright" href="{{base_url()}}">
<meta property="og:image" content="{{$project->imagedir}}">
<meta title="keywords" content="{{$seo->keyword}} , project">
<meta title="description" content="Our Project , {{read_more($project->description,200)}}">
@endsection

@section('styles')
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
@endsection

@section('content')
  <div role="main" class="main">
      <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
                <li><a href="{{base_url()}}">Home</a></li>
                <li><a href="{{base_url()}}/main/project">ARTICLE</a></li>
                <li class="active">{{$project->title}}</li>
                
              </ul>
            </div>
          </div>
        </div>
      </section>
      <div class="container">
        <div class="row">
          <div class="col-md-9">
            <div class="blog-posts single-post">
              <article class="post post-large blog-single-post">
                <div class="post-image">

                  <div class="owl-carousel owl-theme" data-plugin-options="{'items': 1}">
                   @foreach($project->gallery as $image)
                    <div>
                      <center>
                      <div class="img-thumbnail">

                        <img src="{{$image->imagedir}}" class="img-responsive" alt="" >
                       
                      </div>
                      </center>
                    </div>
                    
                    @endforeach
                  </div>
                </div>
              <div class="post-date">
                <!-- fungsi ada di aksa_helper (helper) -->
                <?php
                  $create = explode('-',$project->created_at);
                  
                  $thn    = $create[0];
                  $bln    = $create[1];
                // menampilkan array

// echo $data = date('Y-m-d',strtotime($timestamp));
// echo '<br>';
// echo $time = date('H:i:s',strtotime($timestamp));
//                   2012-10-19
// 18:19:56

// array 2
                  $tgl    = $create[2];
                  // nampilno tgl e tok
                  $tanggal = date('d',strtotime($project->created_at));
                
                 
                ?>
                  <span class="day">{{($tanggal)}}</span>
                  <span class="month">{{bln_indo($bln)}}</span>
                  <span class="year">{{$thn}}</span>
                </div>
                <div class="post-content">
                  <h2>{{$project->title}}</h2>
                  <div class="post-meta">
                   
                  </div>
                  <p>{!!$project->description!!}.</p>
                </div>
                <div class="post-block" id="share_post"></div>
              </article>
            </div>
          </div>
          <div class="col-md-3">
            <aside class="sidebar">
              <form action="{{base_url('main/search')}}" method="get">
                <div class="input-group">
                  <input type="search" name="q" class="form-control" placeholder="Search...">
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                  </span>
                </div>
              </form>
  
            </aside>
            <br>
            <br>
            <br>

          </div>
        </div>
      
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
<script>
 $("#shareIcons").jsSocials({ 
    showLabel: false,
    showCount: false,
    shares: ["twitter", "facebook", "whatsapp","googleplus","line"]
  });
</script>
@endsection