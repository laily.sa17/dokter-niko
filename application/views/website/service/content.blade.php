@extends('website.template')
@section('title')
	{{$service->name}} - {{$seo->title}}
@endsection

@section('meta')
 <meta name="author" content="{{$config->name}}">
<link rel="copyright" href="{{base_url()}}">
<meta property="og:image" content="{{$service->imagedir}}">
<meta name="keywords" content="{{$seo->keyword}} , service">
<meta name="description" content="Our Service , {{read_more($service->description,200)}}">
@endsection

@section('styles')
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
@endsection

@section('content')
  <div role="main" class="main">
      <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
                <li><a href="index.html">Detail</a></li>
                <li class="active">{{$service->name}}</li>
                <li>created: {{$service->created_at}}</li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <div class="container">
        <div class="row">
          <div class="col-md-9">
            <div class="blog-posts single-post">
              <article class="post post-large blog-single-post">
                <div class="post-image">
                  <div class="owl-carousel owl-theme" data-plugin-options="{'items': 1}">
                    <div>
                      <div class="img-thumbnail">
                        <img src="https://picsum.photos/g/1280/500?image=0" class="img-responsive" alt="">
                      </div>
                    </div>
                    <div>
                      <div class="img-thumbnail">
                        <img src="https://picsum.photos/g/1280/500?image=1" class="img-responsive" alt="">
                      </div>
                    </div>
                    <div>
                      <div class="img-thumbnail">
                        <img src="https://picsum.photos/g/1280/500?image=2" class="img-responsive" alt="">
                      </div>
                    </div>
                  </div>
                </div>

                <div class="post-content">
                  <h2>{{$service->name}}</h2>
                  <div class="post-meta">
                    <span><i class="fa fa-tag"></i> <a href="#">Tag 1</a>, <a href="#">Tag 2</a></span>
                  </div>
                  <p>{!! $service->description !!}</p>
                </div>
                <div class="post-block" id="shareIcons"></div>
              </article>
            </div>
          </div>
          <div class="col-md-3">
            <aside class="sidebar">
              <form>
                <div class="input-group">
                  <input type="text" name="q" class="form-control" placeholder="Cari...">
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                  </span>
                </div>
              </form>
              <hr>
              <h4 class="heading-primary">Kategori</h4>
              <ul class="nav nav-list mb-xlg">
                <li><a href="#">Kategori 1</a></li>
                <li><a href="#">Kategori 2</a></li>
                <li><a href="#">Kategori 3</a></li>
                <li><a href="#">Kategori 4</a></li>
                <li><a href="#">Kategori 5</a></li>
              </ul>
            </aside>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
<script>
 $("#shareIcons").jsSocials({ 
    showLabel: false,
    showCount: false,
    shares: ["twitter", "facebook", "whatsapp","googleplus","line"]
  });
</script>
@endsection