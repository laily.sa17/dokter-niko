@extends('website.template')
@section('title')
	Searching '{{$ctrl->input->get('q')}}'
@endsection



@section('content')
<div id="core" class="core-bg-1">
      <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="index.html">Search</a></li>

                 <li class="active">{{ ucwords($_GET['q']) }}</li>
              </ul>
            </div>
          </div>
        </div>
      </section>
	<!-- PAGE HEADER : begin -->


	<div class="container">
					<div class="row">

						<div class="col-md-6" style="margin-left: 25%">
						<center>
   
						<form class="c-search-form" action="{{base_url('main/search')}}" method="get">
							
							<div class="input-group">
							
								<input  type="search" name="q" placeholder="Search Something Here" class="form-control" value="{{$ctrl->input->get('q')}}">
								  <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                   
                  </span>
                  

							</div>
							
						</form>
</center>
					
			</div>
		
					</div>
				</div>
		
			</div>

						
						<!-- SEARCH FORM : end -->
						<br>
						<br>
						<br>
						<div class="b">
						<div class="container">
<div class="row">
						<!-- DIVIDER : begin -->
						<hr class="c-divider m-medium">
						<!-- DIVIDER : end -->

						<!-- RESULTS LIST : begin -->
						<ul class="results-list">
				@foreach($project_view as $result)
						<div class="col-md-4 mb-50px">
							<span class="thumb-info thumb-info-hide-wrapper-bg">
								<span class="thumb-info-wrapper">
									
										@foreach($result->gallery as $image)
										<a href="{{$result->url}}">
									<img src="{{$image->imagedir}}" class="img-responsive" alt="">
									</a>
										@break
                    			@endforeach
								</span>
								<?php $t = date('Y-m-d',strtotime($result->created_at)); ?>
								<span class="thumb-info-wrapper">
									<span class="thumb-info-caption-text">
										<small><i class="fa fa-calendar"></i> {{tgl_indo($t)}}</small>
										<h4><a href="{{$result->url}}">{!!read_more($result->title, 35)!!}</a></h4>
									<p>{!! read_more($result->description,200) !!}</p>
									</span>
									
								</span>
								<span class="thumb-info-social-icons">
										<center><a href="{{$result->url}}" class="btn btn-3d btn-tertiary">Read</a></center>
									</span>
							</span>
						</div>
							@endforeach	
							
							@foreach($product as $result)
						<div class="col-md-4 mb-50px">
						
							<span class="thumb-info thumb-info-hide-wrapper-bg">
								<span class="thumb-info-wrapper">
									
										@foreach($result->images as $image)
										<a href="{{$result->url}}">
									<img src="{{$image->imagedir}}" class="img-responsive" alt="">
									</a>
										@break
                   					 @endforeach
								</span>
								<?php $t = date('Y-m-d',strtotime($result->created_at)); ?>
								<span class="thumb-info-wrapper">
									<span class="thumb-info-caption-text">
										<small><i class="fa fa-calendar"></i> {{tgl_indo($t)}}</small>
										<h4><a href="{{$result->url}}">{!!read_more($result->name, 35)!!}</a></h4>
									<p>{!! read_more($result->description,200) !!}</p>
									</span>
									
								</span>
								<span class="thumb-info-social-icons">
										<center><a href="{{$result->url}}" class="btn btn-3d btn-tertiary">Read</a></center>
									</span>
							</span>
						
						</div>
							@endforeach	

							
						
						</ul>
					</div>
					</div>
				</div>
				</div>
			</div>

</div>

<!-- CORE : end -->

<!-- BOTTOM PANEL : begin -->
<!-- BOTTOM PANEL : end -->
@endsection
@section('script')

@endsection