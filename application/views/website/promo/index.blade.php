@extends('website.template')
@section('title')
	OUR PROMO - {{$config->name}}
@endsection

@section('content')
  <div id="page-title" class="page-title-parallax text-light" data-stellar-background-ratio="0.9" style="background-image: url('{{base_url()}}images/website/banner.jpg')">
    <div class="background-overlay"></div>
    <div class="container">
      <div class="page-title col-md-8">
        <h1 class="text-uppercase text-medium">promo</h1>
      </div>
    </div>
  </div>

  <section class="content">
    <div class="container">
      <div class="row">
        <div class="post-content post-thumbnail col-md-9">
          <div class="post-item">
          	@foreach($promos as $result)
            <div class="post-content-details">
              <div class="post-title">
                <h3><a href="#">{{$result->name}}</a></h3>
                @foreach($result->promo->where("status",0) as $toko)
                <h4><a href="{{$toko->url}}">{{$toko->name}}</a></h4>
                @endforeach
              </div>

              <div class="post-description">
                &nbsp;               
              </div>

            </div>
            @endforeach
          </div>
        </div>
        <div class="sidebar sidebar-modern col-md-3">
        @include('website.sidebar')
        </div>
      </div>
    </div>
  </section>

@endsection