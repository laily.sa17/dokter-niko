@extends('admin.template')

@section('title')
{{($type=="create") ? 'Buat Popup Baru' : 'Ubah Popup' }} - Administrasi
@endsection

@section('content')
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Website</span> - Popup</h4>
		</div>

	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{base_url('superuser/popup')}}"><i class="icon-stack2 position-left"></i> Popup</a></li>
			<li class="active">Buat Popup</li>
		</ul>
	</div>
</div>

<div class="content">
	<h6 class="content-group text-semibold">
		<span class="text-primary"><i class="icon-magazine"></i> {{ ($type=="create") ? 'Buat' : 'Ubah' }}</span> Popup
		<small class="display-block">
			{{ ($type=="create") ? 'Buat Popup Baru' : 'Perbarui Popup' }}
		</small>
	</h6>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">Data Popup</h6>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<div class="col-md-12 clearfix" style="margin-top:10px;margin-bottom:10px;float:none;padding:20px;">
					<form id="form-blog" class="form-horizontal" 
						action="{{ ($type=='create') ? base_url('superuser/popup/created') : base_url('superuser/popup/updated/'.$popup->id) }}" 
					method="post">

						<div id="popup">
							<div class="form-group">
								<label class="col-lg-2 control-label">Judul Popup <span class="text-danger"><b>*</b></span></label>
								<div class="col-lg-10">
									<input class="form-control" type="text" placeholder="Judul Popup" name="title" 
									value="{{ ($type=='create') ? '' : $popup->title }}" required>
								</div>
							</div>

							<div class="form-group">
								<label class="col-lg-2 control-label">URL Popup <span class="text-danger"><b>*</b></span></label>
								<div class="col-lg-10">
									<input class="form-control" type="text" placeholder="URL Popup" name="url" 
									value="{{ ($type=='create') ? '' : $popup->url }}" required>
								</div>
							</div>

							<div class="form-group">
								<label class="col-lg-2 control-label">Deskripsi Popup <span class="text-danger"><b>*</b></span></label>
								<div class="col-lg-10">
									<textarea  id="editor-full" rows="2"  name="description" cols="2">{{ ($type=='create') ? '' : $popup->description }}</textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="col-lg-2 control-label">Sampul Popup<span class="text-danger"><b>*</b></span></label>
								<div class="col-lg-10">
									<input type="file" class="file-input-custom" name="image" accept="image/*" {{ ($type=='create') ? 'required' : '' }} >
										<span class="help-block"> {{ ($type=='create') ? '' : '( Jangan Ubah Jika Tidak Ada Perubahan)' }}  
										Ukuran Maksimum 2MB , Hanya File Gambar
										</span>
								</div>
									<input type="hidden" name="popup" value="{{ ($type!='create') ? $popup->image : '' }}" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Status <span class="text-danger"> <b>*</b></span></label>
							<div class="col-lg-10">
								<div class="checkbox checkbox-switch">
									<label>
									@if($type == 'create')
										<input type="checkbox" name="aktif" value="0" class="switch" data-on-text="Aktif" data-off-text="Non Aktif" data-on-color="success" data-off-color="default">
										Aktif / Non Aktif
									@else
										<input type="checkbox" name="aktif" value="0" class="switch" data-on-text="Aktif" data-off-text="Non Aktif" data-on-color="success" data-off-color="default" {{(@$popup->aktif==0) ? 'checked' : ''}} >
										Aktif / Non Aktif
									@endif
									</label>
								</div>
							</div>
						</div>
						<div class="text-right">
							<button type="submit" class="btn btn-primary">{{ ($type=='create') ? 'Buat Popup' : 'Ubah Popup' }} <i class="icon-arrow-right14 position-right"></i></button>
							@if($type=="update")
							<a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); "> Batalkan <i class="fa fa-times position-right"></i></a>
							@endif
						</div>
					</form>
				</div>
            </div>
		</div>
	</div>
	<!-- /main charts -->

	<!-- Footer -->

<!-- /footer -->
</div>

@endsection

@section('script')

<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/uploaders/fileinput.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/forms/selects/select2.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/form_layouts.js"></script>

<script type="text/javascript">
	var editorsmall = true;
</script>
<script type="text/javascript" src="{{base_url()}}admin_assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/editor_ckeditor.js"></script>

<script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/uploader_bootstrap.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/forms/styling/switch.min.js"></script>
<script type="text/javascript">
	$(".switch").bootstrapSwitch();	
	$("input[name=popup]").trigger('change');
	function typePopup(that){
		if($(that).is(':checked')){
			$("#other").fadeOut(function(){
				$("select[name=content]").prop("required",false);
				$("input[name=title]").prop('required',true);
				$("#popup").fadeIn();
			})
		}
		else {
			$("#popup").fadeOut(function(){
				$("select[name=content]").prop("required",true);
				$("input[name=title]").prop('required',false);
				$("#other").fadeIn();
			})
		}
	}

	// $(document).ready(function(){
	$('.file-input-custom').fileinput({
        previewFileType: 'image',
        browseLabel: 'Select',
        browseClass: 'btn bg-slate-700',
        browseIcon: '<i class="icon-image2 position-left"></i> ',
        removeLabel: 'Remove',
        removeClass: 'btn btn-danger',
        removeIcon: '<i class="icon-cancel-square position-left"></i> ',
        uploadClass: 'hidden',
        uploadIcon: '<i class="icon-file-upload position-left"></i> ',
        layoutTemplates: {
            caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
        },
        initialPreview: ["<img src='{{ ($type=='create') ? img_holder() : $popup->imagedir }}' class='file-preview-image' alt=''>",],
        overwriteInitial: true
    });
	// })


	$("#form-blog").submit(function(e){
			e.preventDefault();
			var formData = new FormData( $("#form-blog")[0] );

			 for ( instance in CKEDITOR.instances ) {
		        CKEDITOR.instances[instance].updateElement();
		    }

			$.ajax({
				url: 		$("#form-blog").attr('action'),
				method: 	"POST",
				data:  		new FormData(this),
          		processData: false,
          		contentType: false,
				beforeSend: function(){
					blockMessage($('#form-blog'),'Please Wait , {{ ($type =="create") ? "Menambahkan Popup" : "Memperbarui Popup" }}','#fff');		
				}
			})
			.done(function(data){
				$('#form-blog').unblock();
				sweetAlert({
					title: 	((data.auth==false) ? "Opps!" : '{{ ($type =="create") ? "Popup Di Buatkan" : "Popup Di Perbarui" }}'),
					text: 	data.msg,
					type: 	((data.auth==false) ? "error" : "success"),
				},
				function(){
					if(data.auth!=false){
						redirect("{{base_url('superuser/popup')}}");		
						return;
					}
					//redirect('{{ ($type =="create") ?  base_url("administrasi/popup/add") : base_url("administrasi/popup/edit/".$popup->id."/".seo($popup->title))}}');
				});
				//redirect("{{base_url('dashboard/promo/list')}}");

			})
			.fail(function() {
			    $('#form-blog').unblock();
				sweetAlert({
					title: 	"Opss!",
					text: 	"Ada Yang Salah! , Silahkan Coba Lagi Nanti",
					type: 	"error",
				},
				function(){
					/*redirect('{{ ($type =="create") ?  base_url("administrasi/popup/create") : base_url("administrasi/popup/update/".$popup->id."/".seo($popup->title))}}');*/
				});
			 })
			
		})

</script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/form_select2.js"></script>
@endsection
