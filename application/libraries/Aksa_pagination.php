<?php 
class Aksa_pagination {
	
	function __construct(){
		$this->ci =&get_instance();
		$this->ci->load->library('pagination');
	}

	function paginate($url, $numlinks, $per_page, $number_row,$page=0){

		# iki nggolek onok pirang page
		# misal onok 
		$total_rows = ceil($number_row/$per_page);

		$config = [
			'base_url' => $url,
			'total_rows' => $total_rows,
			'per_page' => 1,
			//'uri_segment' => $uri_segment,
			'first_link' => false,
			'last_link' => false,
					//'uri_segment' => $uri_segment,
	'next_link'        => 'Next',
       'prev_link'        => 'Prev',
      'full_tag_open'    => '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">',
       'full_tag_close'   => '</ul></nav></div>',
       'num_tag_open'     => '<li class="page-item"><span class="page-link">',
       'num_tag_close'   => '</span></li>',
      'cur_tag_open'     => '<li class="page-item active"><span class="page-link">',
       'cur_tag_close'    => '<span class="sr-only">(current)</span></span></li>',
       'next_tag_open'    => '<li class="page-item"><span class="page-link">',
        'next_tagl_close'  => '<span aria-hidden="true">&raquo;</span></span></li>',
      'prev_tag_open'    => '<li class="page-item"><span class="page-link">',
       'prev_tagl_close'  => '</span>Next</li>',
       'first_tag_open'   => '<li class="page-item"><span class="page-link">',
       'first_tagl_close' => '</span></li>',
       'last_tag_open'    => '<li class="page-item"><span class="page-link">',
       'last_tagl_close'  => '</span></li>'
		];

		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);

		$this->ci->pagination->initialize($config);
		return $this->ci->pagination->create_links();

	}
}