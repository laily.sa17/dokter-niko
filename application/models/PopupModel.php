<?php

class PopupModel extends MY_Model
{
	protected $table 	= "popup";
	protected $appends 	= array('imagedir');

	public function getImagedirAttribute()
	{
		if (!$this->image || !file_exists("images/popup/{$this->image}")) {
			return base_url(img_holder());
		}

		return base_url("images/popup/{$this->image}");
	}

	public function scopeAktif($query){
		return $query->where("aktif",0);
	}

}
