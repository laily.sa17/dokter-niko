<?php

class PromoModel extends MY_Model
{
	protected $table 	= "promo";
	protected $appends 	= array('imagedir','url','urlupdate','urldelete');

    public function store()
	{
		return $this->hasOne('StoreModel', 'id', 'id_store');
	}

	public function getImagedirAttribute()
	{
		if (!$this->image|| !file_exists("images/promo/{$this->image}")) {
			return img_holder();
		}

		return base_url("images/promo/{$this->image}");	
	}

	public function getUrlAttribute()
	{			
		return base_url("main/promo/detail/{$this->id}/".seo($this->name));	
	}

	public function getUrlupdateAttribute()
	{
		return base_url('superuser/promo/update/'.$this->id);
	}

	public function getUrldeleteAttribute()
	{
		return base_url('superuser/promo/delete/'.$this->id);
	}

	public function scopeNotDraft($query){
		return $query->where("status",0);
	}
}