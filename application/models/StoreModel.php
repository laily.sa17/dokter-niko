<?php

class StoreModel extends MY_Model
{
	protected $table 	= "store";
	protected $appends 	= array('url','urlupdate','urldelete','imagedir');

    public function promo()
	{
		return $this->hasMany('PromoModel', 'id_store', 'id');
	}

	public function getImagedirAttribute()
	{		
		if (!$this->image|| !file_exists("images/store/{$this->image}")) {
			return img_holder();
		}

		return base_url("images/store/{$this->image}");	
	}

	public function getUrlAttribute()
	{			
		return base_url("main/store/detail/{$this->id}/".seo($this->name));
	}

	public function getUrlupdateAttribute()
	{
		return base_url('superuser/store/update/'.$this->id.'/'.seo($this->name));
	}

	public function getUrldeleteAttribute()
	{
		return base_url('superuser/store/delete/'.$this->id);
	}
}